package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    // TODO Complete me!
    public ModelDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyNoWay();
    }
    public void display() {
        System.out.println("I am Model Duck");
    }
}
